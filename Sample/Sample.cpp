// *****************************************************************************
//
//      Copyright (c) 2013, Pleora Technologies Inc., All rights reserved.
//
// *****************************************************************************

//
// Shows how to use a PvStream object to acquire images from a GigE Vision or
// USB3 Vision device.
//

#include <PvSampleUtils.h>
#include <PvDevice.h>
#include <PvDeviceGEV.h>
#include <PvStream.h>
#include <PvStreamGEV.h>
#include <PvBuffer.h>

#include <list>
#include <stdio.h>
typedef std::list<PvBuffer *> BufferList;

PV_INIT_SIGNAL_HANDLER();

#define BUFFER_COUNT ( 16 )

///
/// Function Prototypes
///
PvDevice *ConnectToDevice( const PvString &aConnectionID );
PvStream *OpenStream( const PvString &aConnectionID );
void ConfigureStream( PvDevice *aDevice, PvStream *aStream );
void CreateStreamBuffers( PvDevice *aDevice, PvStream *aStream, BufferList *aBufferList );
void AcquireImages( PvDevice *aDevice, PvStream *aStream );
void ConfigureDevice( PvDevice *aDevice );
void FreeStreamBuffers( BufferList *aBufferList );

//
// Main function
//
int main(int argc, char **argv)
{
    PvDevice *lDevice = NULL;
    PvStream *lStream = NULL;
    BufferList lBufferList;

    //  
    if (argc < 2) {
        cout << "Bitte IP als 1. Parameter angeben" << endl;
        return 1;
    }

    PvString lConnectionID = PvString(argv[1]);

    cout << "Connecting to " << static_cast<string>(lConnectionID) << endl << endl;

    lDevice = ConnectToDevice( lConnectionID );
    if ( NULL != lDevice )
    {
        // ...
        ConfigureDevice(lDevice);

        lStream = OpenStream( lConnectionID );
        if ( NULL != lStream )
        {
            ConfigureStream( lDevice, lStream );
            CreateStreamBuffers( lDevice, lStream, &lBufferList );
            AcquireImages( lDevice, lStream );
            FreeStreamBuffers( &lBufferList );
            
            // Close the stream
            cout << "Closing stream" << endl;
            lStream->Close();
            PvStream::Free( lStream );    
        }

        // Disconnect the device
        cout << "Disconnecting device" << endl;
        lDevice->Disconnect();
        PvDevice::Free( lDevice );
    }

    cout << endl;

    return 0;
}

PvDevice *ConnectToDevice( const PvString &aConnectionID )
{
    PvDevice *lDevice;
    PvResult lResult;

    // Connect to the GigE Vision or USB3 Vision device
    cout << "Connecting to device." << endl;
    lDevice = PvDevice::CreateAndConnect( aConnectionID, &lResult );
    if ( lDevice == NULL )
    {
        cout << "Unable to connect to device." << endl;
    }

    return lDevice;
}

PvStream *OpenStream( const PvString &aConnectionID )
{
    PvStream *lStream;
    PvResult lResult;

    // Open stream to the GigE Vision or USB3 Vision device
    cout << "Opening stream from device." << endl;
    lStream = PvStream::CreateAndOpen( aConnectionID, &lResult );
    if ( lStream == NULL )
    {
        cout << "Unable to stream from device." << endl;
    }

    return lStream;
}

void ConfigureStream( PvDevice *aDevice, PvStream *aStream )
{
    // If this is a GigE Vision device, configure GigE Vision specific streaming parameters
    PvDeviceGEV* lDeviceGEV = dynamic_cast<PvDeviceGEV *>( aDevice );
    if ( lDeviceGEV != NULL )
    {
        PvStreamGEV *lStreamGEV = static_cast<PvStreamGEV *>( aStream );

        // Negotiate packet size
        lDeviceGEV->NegotiatePacketSize();

        // Configure device streaming destination
        lDeviceGEV->SetStreamDestination( lStreamGEV->GetLocalIPAddress(), lStreamGEV->GetLocalPort() );
    }
}

void CreateStreamBuffers( PvDevice *aDevice, PvStream *aStream, BufferList *aBufferList )
{
    // Reading payload size from device
    uint32_t lSize = aDevice->GetPayloadSize();

    // Use BUFFER_COUNT or the maximum number of buffers, whichever is smaller
    uint32_t lBufferCount = ( aStream->GetQueuedBufferMaximum() < BUFFER_COUNT ) ? 
        aStream->GetQueuedBufferMaximum() :
        BUFFER_COUNT;

    // Allocate buffers
    for ( uint32_t i = 0; i < lBufferCount; i++ )
    {
        // Create new buffer object
        PvBuffer *lBuffer = new PvBuffer;

        // Have the new buffer object allocate payload memory
        lBuffer->Alloc( static_cast<uint32_t>( lSize ) );
        
        // Add to external list - used to eventually release the buffers
        aBufferList->push_back( lBuffer );
    }
    
    // Queue all buffers in the stream
    BufferList::iterator lIt = aBufferList->begin();
    while ( lIt != aBufferList->end() )
    {
        aStream->QueueBuffer( *lIt );
        lIt++;
    }
}

void ConfigureDevice( PvDevice *aDevice)
{
    // Get device parameters need to control streaming
    PvGenParameterArray *lDeviceParams = aDevice->GetParameters();

    PvGenParameter *lTriggerMode = lDeviceParams->Get( "PSL_TRIGGER_MODE" );
    cout << "Trigger mode set to: " << static_cast<string>(lTriggerMode->ToString()) << endl;
}

void SaveImage( uint8_t *data, size_t len, uint32_t num)
{
    // save image to disk
    FILE *pfile;

    char filename[256];
    
    sprintf(filename, "image_%04d.raw", num);

    pfile = fopen(filename, "wb");
    fwrite(data, sizeof(uint8_t), len, pfile);
    fclose(pfile);
}

void AcquireImages( PvDevice *aDevice, PvStream *aStream )
{
    // Get device parameters need to control streaming
    PvGenParameterArray *lDeviceParams = aDevice->GetParameters();

    // Map the GenICam AcquisitionStart and AcquisitionStop commands
    PvGenCommand *lStart = dynamic_cast<PvGenCommand *>( lDeviceParams->Get( "AcquisitionStart" ) );
    PvGenCommand *lStop = dynamic_cast<PvGenCommand *>( lDeviceParams->Get( "AcquisitionStop" ) );

    PvGenCommand *lTrigger = dynamic_cast<PvGenCommand *> ( lDeviceParams->Get( "TriggerSoftware" ) );


    // Get stream parameters
    PvGenParameterArray *lStreamParams = aStream->GetParameters();

    // Map a few GenICam stream stats counters
    PvGenFloat *lFrameRate = dynamic_cast<PvGenFloat *>( lStreamParams->Get( "AcquisitionRate" ) );
    PvGenFloat *lBandwidth = dynamic_cast<PvGenFloat *>( lStreamParams->Get( "Bandwidth" ) );

    // Enable streaming and send the AcquisitionStart command
    cout << "Enabling streaming and sending AcquisitionStart command." << endl;
    aDevice->StreamEnable();
    lStart->Execute();

    // Acquire images until the user instructs us to stop.
    cout << endl << "<press a ^C to stop streaming>" << endl;

    // ctrl-c sets gStop
    while ( !gStop )
    {
        PvBuffer *lBuffer = NULL;
        PvResult lOperationResult;

        if(PvKbHit()) {
          PvGetChar(); // Flush key buffer for next stop.
          cout << "Sending software trigger." << endl;
    	  lTrigger->Execute();
        } 

        // Retrieve next buffer
        PvResult lResult = aStream->RetrieveBuffer( &lBuffer, &lOperationResult, 1000 );
        if ( lResult.IsOK() )
        {
            if ( lOperationResult.IsOK() )
            {
                PvPayloadType lType;

                //
                // We now have a valid buffer. This is where you would typically process the buffer.
                // -----------------------------------------------------------------------------------------
                // ...

                // If the buffer contains an image, display width and height.
                uint32_t lWidth = 0, lHeight = 0;
                uint32_t blockid;
                lType = lBuffer->GetPayloadType();
                blockid = lBuffer->GetBlockID();

                cout << fixed << setprecision( 1 );
                cout << " BlockID: " << uppercase << hex << setfill( '0' ) << setw( 16 ) << blockid;
                if ( lType == PvPayloadTypeImage )
                {
                    // Get image specific buffer interface.
                    PvImage *lImage = lBuffer->GetImage();

                    // Read width, height.
                    lWidth = lImage->GetWidth();
                    lHeight = lImage->GetHeight();
                    cout << "  W: " << dec << lWidth << " H: " << lHeight << endl;

                    // write to disk
                    uint8_t *data = lImage->GetDataPointer(); 
                    uint32_t s_data = lImage->GetImageSize();
                    SaveImage(data, s_data, blockid);
                }
                else 
                {
                    cout << " (buffer does not contain image)";
                }
            }
            else
            {
                // Non OK operational result
                cout << "Operation result: " << lOperationResult.GetCodeString().GetAscii() << endl;
            }

            // Re-queue the buffer in the stream object
            aStream->QueueBuffer( lBuffer );
        }
        else
        {
            // Retrieve buffer failure
            cout << "Buffer failure: " << lResult.GetCodeString().GetAscii() << endl;
        }
    }

    PvGetChar(); // Flush key buffer for next stop.
    cout << endl << endl;

    // Tell the device to stop sending images.
    cout << "Sending AcquisitionStop command to the device" << endl;
    lStop->Execute();

    // Disable streaming on the device
    cout << "Disable streaming on the controller." << endl;
    aDevice->StreamDisable();

    // Abort all buffers from the stream and dequeue
    cout << "Aborting buffers still in stream" << endl;
    aStream->AbortQueuedBuffers();
    while ( aStream->GetQueuedBufferCount() > 0 )
    {
        PvBuffer *lBuffer = NULL;
        PvResult lOperationResult;

        aStream->RetrieveBuffer( &lBuffer, &lOperationResult );
    }
}

void FreeStreamBuffers( BufferList *aBufferList )
{
    // Go through the buffer list
    BufferList::iterator lIt = aBufferList->begin();
    while ( lIt != aBufferList->end() )
    {
        delete *lIt;
        lIt++;
    }

    // Clear the buffer list 
    aBufferList->clear();
}


