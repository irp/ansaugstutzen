# Sample

Aufruf: `./Sample <IP>`

Beenden mit ^C.

 - erstellt ein PvDevice
 - stellt eine Verbindung her und konfiguriert diese
 - konfiguriert das Device
 - Startet einen Stream
 - Sendet Software Trigger bei Tastendruck
 - Zeigt empfangene Bilder an und speichert diese in image_xxxx.raw
 
Beispiel Pirra:

Mit master verbinden
```
./Sample 160.255.1.2
```

Mit slave verbinden
```
./Sample 160.255.2.2
```

Nur der Master hoert auf Software Trigger.
