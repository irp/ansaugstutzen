#pragma once

#include <PvDevice.h>
#include <PvDeviceGEV.h>
#include <PvStream.h>
#include <PvStreamGEV.h>
#include <PvBuffer.h>

#include <list>

#include "TiffWriter.hpp"

typedef std::list<PvBuffer> BufferList;

class CameraChip {

  private:
    PvString connectionID;

    PvDevice *lDevice;
    PvStream *lStream;
    BufferList lBufferList;

    PvGenParameterArray *lDeviceParams;

    PvGenCommand *lStart;
    PvGenCommand *lStop;
    PvGenCommand *lTrigger;

  public:
    CameraChip(const char *);
    ~CameraChip();

    PvBuffer *AcquireImages();

    // TODO: replace with generic handler
    TiffWriter *image_handler;

    void ConnectToDevice();
    void OpenStream();
    void StartAcquisition();
    void StopAcquisition();
    void SoftwareTrigger();
    void QueueBuffer(PvBuffer *buf);
    void SetDeviceParameterInteger(const char *name, int64_t value);
    void SetDeviceParameterEnum(const char *name, const char *value);
};
