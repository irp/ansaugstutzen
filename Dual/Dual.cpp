#include <PvSampleUtils.h>

#include <cstdio>
#include <cstdlib>

#include "Camerachip.hpp"
#include "TiffWriter.hpp"

PV_INIT_SIGNAL_HANDLER();

//
// Main function
//
int main(int argc, char **argv)
{

    if (argc < 3) {
        cout << "Bitte IPs als 1. und 2. Parameter angeben" << endl;
        return 1;
    }

    // setup camera 1
    CameraChip *cam1 = new CameraChip(argv[1]);
    CameraChip *slave = cam1;
    uint32_t cam1_counter = 0;

    slave->ConnectToDevice();

    slave->setDeviceParameterInteger("Width", 2048);
    slave->setDeviceParameterInteger("Height", 1024);
    slave->setDeviceParameterEnum("PixelFormat", "Mono16");
    slave->setDeviceParameterInteger("AcquisitionFrameCount", 1);
    slave->setDeviceParameterInteger("ExposureTimeRaw", 100000);
    slave->setDeviceParameterEnum("PSL_TRIGGER_MODE", "SW_Trigger");

    slave->OpenStream();

    // setup camera 2
    CameraChip *cam2 = new CameraChip(argv[2]);
    CameraChip *master = cam2;
    uint32_t cam2_counter = 0;

    master->ConnectToDevice();

    master->setDeviceParameterInteger("Width", 2048);
    master->setDeviceParameterInteger("Height", 1024);
    master->setDeviceParameterEnum("PixelFormat", "Mono16");
    master->setDeviceParameterInteger("AcquisitionFrameCount", 1);
    master->setDeviceParameterInteger("ExposureTimeRaw", 100000);
    master->setDeviceParameterEnum("PSL_TRIGGER_MODE", "SW_Trigger");

    master->OpenStream();

    cam1->StartAcquisition();
    cam2->StartAcquisition();

    /* Strategy:
     * Retrieve buffers containing images and enqueue them into a processing queue.
     *
     * For now, simply write them to disk. Later, the processing queue could be realized in another thread.
     */

    // ctrl-c sets gStop
    while ( !gStop ) {

        if(PvKbHit()) {
          PvGetChar(); // Flush key buffer for next stop.
          cout << "Sending software trigger..." << endl;

          master->SoftwareTrigger(); 
        } 

        PvBuffer *cam1_buf = cam1->AcquireImages();
        PvBuffer *cam2_buf = cam1->AcquireImages();

        if (NULL != cam1_buf) {
          PvImage *lImage = cam1_buf->GetImage();

          // Read width, height for debugging purposes. They *must* be as we set them.
          uint32_t lWidth = lImage->GetWidth();
          uint32_t lHeight = lImage->GetHeight();
          cout << "  W: " <<  lWidth << " H: " << lHeight << endl;

          // write to buffer
          uint8_t *imbuf = lImage->GetDataPointer(); 
          size_t buflen = lImage->GetImageSize();

          char cam1_prefix[] = "cam1";
          write_tiff(imbuf, buflen, cam1_prefix, cam1_counter, lWidth, lHeight);
          cam1_counter++;

          cam1->QueueBuffer(cam1_buf);
        }

        if (NULL != cam2_buf) {
          PvImage *lImage = cam2_buf->GetImage();

          // Read width, height for debugging purposes. They *must* be as we set them.
          uint32_t lWidth = lImage->GetWidth();
          uint32_t lHeight = lImage->GetHeight();
          cout << "  W: " <<  lWidth << " H: " << lHeight << endl;

          // write to buffer
          uint8_t *imbuf = lImage->GetDataPointer(); 
          size_t buflen = lImage->GetImageSize();

          char cam2_prefix [] = "cam2";
          write_tiff(imbuf, buflen, cam2_prefix, cam2_counter, lWidth, lHeight);
          cam2_counter++;

          cam2->QueueBuffer(cam2_buf);
        }
    }


    cam1->StopAcquisition();
    cam2->StopAcquisition();

    delete cam1;
    delete cam2;

    return 0;
}

