#include "Camerachip.hpp"
#include <spdlog/spdlog.h>

using namespace std;

void setParameter(PvGenInteger * parameter, int64_t value);
void setParameter(PvGenEnum * parameter, const char * value);

const uint32_t TIMEOUT = 10;      // timeout in ms
const size_t   BUFFER_COUNT = 16; // max. number of receive buffers

auto _logger = spdlog::stdout_logger_mt("console");

CameraChip::CameraChip( const char *aConnectionID) {

  _logger->debug("Creating {} ...", aConnectionID);
  connectionID = PvString(aConnectionID);

  ConnectToDevice();
}

CameraChip::~CameraChip() {
  // free stream buffers
  delete lBufferList;

  if (NULL != lStream) {
    // Close the stream
    _logger->debug("Closing stream");
    lStream->Close();
    PvStream::Free(lStream);    
  }

  if (NULL != lDevice) {
    // Disconnect the device
    _logger->debug("Disconnecting device");
    lDevice->Disconnect();
    PvDevice::Free(lDevice);
  }
}

void CameraChip::OpenStream() {
  PvResult lResult;

  // Open stream to the GigE Vision or USB3 Vision device
  _logger->debug("Opening stream from device");
  lStream = PvStream::CreateAndOpen( connectionID, &lResult );
  if (NULL == lStream) {
    _logger->critical("Unable to stream from device");
  } else {
    // If this is a GigE Vision device, configure GigE Vision specific streaming parameters
    PvDeviceGEV* lDeviceGEV = dynamic_cast<PvDeviceGEV *>( lDevice );
    if (NULL != lDeviceGEV) {
      PvStreamGEV *lStreamGEV = static_cast<PvStreamGEV *>( lStream );

      // Negotiate packet size
      lDeviceGEV->NegotiatePacketSize();

      // Configure device streaming destination
      lDeviceGEV->SetStreamDestination( lStreamGEV->GetLocalIPAddress(), lStreamGEV->GetLocalPort() );
    }

    // Reading payload size from device
    uint32_t lSize = lDevice->GetPayloadSize();

    // Use BUFFER_COUNT or the maximum number of buffers, whichever is smaller
    uint32_t lBufferCount = ( lStream->GetQueuedBufferMaximum() < BUFFER_COUNT ) ? 
        lStream->GetQueuedBufferMaximum() :
        BUFFER_COUNT;

    // Create buffers
    for ( uint32_t i = 0; i < lBufferCount; i++ )
    {
      // Create buffer object
      PvBuffer lBuffer;

      // Have the new buffer object allocate payload memory
      lBuffer->Alloc(aSize);

      // Add to external list - used to eventually release the buffers
      aBufferList->push_back( lBuffer );
    }

    // Queue all buffers in the stream
    BufferList::iterator lIt = lBufferList.begin();
    while ( lIt != lBufferList.end() ) {
      lStream->QueueBuffer( *lIt );
      lIt++;
    }
  }
}

void CameraChip::SetDeviceParameterInteger(const char *name, int64_t value) {
  PvGenInteger * parameter = dynamic_cast<PvGenInteger *>(lDeviceParams->Get(name));
  setParameter(parameter, value);
}

void CameraChip::SetDeviceParameterEnum(const char *name, const char *value) {
  PvGenEnum * parameter = dynamic_cast<PvGenEnum *>(lDeviceParams->Get(name));
  setParameter(parameter, value);
}


void CameraChip::StartAcquisition() {
  // Enable streaming and send the AcquisitionStart command
  _logger->set_level(spdlog::level::debug);
  _logger->debug("Enabling streaming and sending AcquisitionStart command.");
  lDevice->StreamEnable();
  lStart->Execute();
}

void CameraChip::StopAcquisition() {
  // Tell the device to stop sending images.
  _logger->debug( "Sending AcquisitionStop command to the device");
  lStop->Execute();

  // Disable streaming on the device
  _logger->debug("Disable streaming on the controller.");
  lDevice->StreamDisable();

  // Abort all buffers from the stream and dequeue
  _logger->debug( "Aborting buffers still in stream");
  lStream->AbortQueuedBuffers();
  while ( lStream->GetQueuedBufferCount() > 0 ) {
    PvBuffer *lBuffer = NULL;
    PvResult lOperationResult;

    lStream->RetrieveBuffer( &lBuffer, &lOperationResult );
  }
}

PvBuffer * CameraChip::AcquireImages() {
  PvBuffer *lBuffer = NULL;
  PvResult lOperationResult;

  // Retrieve next buffer if one is available
  if (lStream->GetQueuedBufferCount() <= 0) {
    return NULL;
  }
  PvResult lResult = lStream->RetrieveBuffer( &lBuffer, &lOperationResult, 100);
  if ( lResult.IsOK() ) {
    if ( lOperationResult.IsOK() ) {
      PvPayloadType lType;

      lType = lBuffer->GetPayloadType();
      uint32_t blockid = lBuffer->GetBlockID();

      _logger->debug("Block received. BlockID: {}", blockid);
      if ( lType == PvPayloadTypeImage ) {
        return lBuffer;
      } else {
        _logger->debug("Buffer does not contain image");
      }
    } else {
      // Non OK operational result
      _logger->warn("Operation result: {}", lOperationResult.GetCodeString().GetAscii());
    }

    // Re-queue the buffer in the stream object
    lStream->QueueBuffer( lBuffer );
  } else if( lResult.GetCode() == PvResult::Code::TIMEOUT ) {
    // ignore
  } else {
    // Retrieve buffer failure
    _logger->error("Buffer failure: {}", lResult.GetCodeString().GetAscii());
  }

  return NULL;
}

void CameraChip::QueueBuffer(PvBuffer *buf) {
    lStream->QueueBuffer(buf);
}

void CameraChip::ConnectToDevice() {
  PvResult lResult;

  // Connect to the GigE Vision or USB3 Vision device
  _logger->debug("Connecting to device.");
  lDevice = PvDevice::CreateAndConnect( connectionID, &lResult );
  if (NULL == lDevice) {
    _logger->critical("Unable to connect to device.");
  }else{
    lDeviceParams = lDevice->GetParameters();

    // Section: AquisitionAndTriggerControls
    lStart = dynamic_cast<PvGenCommand *>( lDeviceParams->Get( "AcquisitionStart" ) );
    lStop = dynamic_cast<PvGenCommand *>( lDeviceParams->Get( "AcquisitionStop" ) ); 
    lTrigger = dynamic_cast<PvGenCommand *> ( lDeviceParams->Get( "TriggerSoftware" ) );

    PvString value;
    PvGenString *lModelName =  dynamic_cast<PvGenString *>(lDeviceParams->Get( "DeviceModelName" ));
    lModelName->GetValue(value);
    _logger->info("Device Model Name: {}", value.GetAscii());
    PvGenEnum *lFault =  dynamic_cast<PvGenEnum *>(lDeviceParams->Get( "Fault" ));
    lFault->GetValue(value);
    _logger->info("Device Fault Status: {}", value.GetAscii());

  }
}

void CameraChip::SoftwareTrigger() {
        PvResult lResult = lTrigger->Execute();

        if (!lResult.IsOK()) {
                _logger->error("Software Trigger failed: {}", lResult.GetDescription().GetAscii());
        }else{
                _logger->debug("Software Trigger OK");
        }
}



void CreateStreamBuffers(uint32_t aSize, uint32_t aBufferCount, BufferList *aBufferList) {
  // Allocate buffers
  for ( uint32_t i = 0; i < aBufferCount; i++ )
  {
    // Create new buffer object
    PvBuffer *lBuffer = new PvBuffer;

    // Have the new buffer object allocate payload memory
    lBuffer->Alloc(aSize);

    // Add to external list - used to eventually release the buffers
    aBufferList->push_back( lBuffer );
  }

}

// Convenience functions
void SetParameter(PvGenEnum * parameter, const char * value) {
  PvResult lResult;
  lResult = parameter->SetValue(value);
  if (! lResult.IsOK()) {
    _logger->error("Failed setting `{}' to `{}'", parameter->GetName().GetAscii(), value); 
  }
}

void SetParameter(PvGenInteger * parameter, int64_t value) {
  PvResult lResult;
  lResult = parameter->SetValue(value);
  if (! lResult.IsOK()) {
    _logger->error("Failed setting `{}' to `{}'", parameter->GetName().GetAscii(), value); 
  }
}

