//
// To receive images using PvPipeline
//

#include <PvStream.h>
#include <PvPipeline.h>
#include <PvBuffer.h>
#include <PvStreamGEV.h>

#include <iostream>

const int buffer_count = 16;
const int timeout = 100;
const int payload_size = 20000000;

using namespace std;

//
// PvPipeline event handler class. Used to trap buffer too small events
// and let the pipeline know we want all the buffers to be resized immediately.
//

class PipelineEventSink : public PvPipelineEventSink
{
protected:

    void OnBufferTooSmall( PvPipeline *aPipeline, bool *aReallocAll, bool *aResetStats )
    {
        *aReallocAll = true;
        *aResetStats = true;

        cout << "Buffers reallocated by the pipeline" << endl;
    }
};

bool StartSlave(std::string const& id, std::string const& aIP, int aPort)
{
    // Create the PvStreamGEV object.
    PvStreamGEV lStreamGEV;

    // Create the PvPipeline object.
    PvPipeline lPipeline( &lStreamGEV );
    

    // Create a PvPipeline event sink (used to trap buffer too small events).
    PipelineEventSink lPipelineEventSink;
    lPipeline.RegisterEventSink( &lPipelineEventSink );

    // Open stream
    cout << "Opening stream from " << id << endl;
    
    if ( !lStreamGEV.Open( id.c_str(), aPort ).IsOK() )
    {
        cout << "Error opening stream" << endl;
        return false;
    }

    // IMPORTANT: the pipeline needs to be "armed", or started before 
    // we instruct the device to send us images.
    cout << "Starting pipeline" << endl;
    lPipeline.SetBufferSize(payload_size);
    lPipeline.SetBufferCount( buffer_count );
    lPipeline.Start();

    // Get stream parameters/stats.
    PvGenParameterArray *lStreamParams = lStreamGEV.GetParameters();
    PvGenInteger *lBlockCount = dynamic_cast<PvGenInteger *>( lStreamParams->Get( "BlockCount" ) );
    PvGenFloat *lFrameRate = dynamic_cast<PvGenFloat *>( lStreamParams->Get( "AcquisitionRate" ) );
    PvGenFloat *lBandwidth = dynamic_cast<PvGenFloat *>( lStreamParams->Get( "Bandwidth" ) );
    PvGenBoolean *lRequestMissingPackets = dynamic_cast<PvGenBoolean *>( lStreamParams->Get( "RequestMissingPackets" ) );

    // Disabling request missing packets.
    lRequestMissingPackets->SetValue( false );

    int64_t lBlockCountVal = 0;
    double lFrameRateVal = 0.0;
    double lBandwidthVal = 0.0;

    // Acquire images until the user instructs us to stop.
    cout << endl;
    while ( true )
    {
		// handle commands here
		
        // Retrieve next buffer     
        PvBuffer *lBuffer = NULL;
        PvResult  lOperationResult;
        PvResult lResult = lPipeline.RetrieveNextBuffer( &lBuffer, timeout, &lOperationResult );
        
        if ( lResult.IsOK() )
        {
            if (lOperationResult.IsOK())
            {
                //
                // We now have a valid buffer. This is where you would typically process the buffer.
                // -----------------------------------------------------------------------------------------
                // ...

                lBlockCount->GetValue( lBlockCountVal );
                lFrameRate->GetValue( lFrameRateVal );
                lBandwidth->GetValue( lBandwidthVal );
            
                // If the buffer contains an image, display width and height.
                uint32_t lWidth = 0, lHeight = 0;
                if ( lBuffer->GetPayloadType() == PvPayloadTypeImage )
                {
                    // Get image specific buffer interface.
                    PvImage *lImage = lBuffer->GetImage();

                    // Read width, height
                    lWidth = lImage->GetWidth();
                    lHeight = lImage->GetHeight();
                }
            
                //cout << fixed << setprecision( 1 );
                //cout << lDoodle[ lDoodleIndex ];
                cout << " BlockID: " << uppercase << hex << lBuffer->GetBlockID() << " W: " << dec << lWidth << " H: " 
                     << lHeight << " " << lFrameRateVal << " FPS " << ( lBandwidthVal / 1000000.0 ) << " Mb/s" << endl;
            }

            // We have an image - do some processing (...) and VERY IMPORTANT,
            // release the buffer back to the pipeline.
            lPipeline.ReleaseBuffer( lBuffer );
        }
        else
        {
            // Timeout
            //cout << " Timeout" << endl;
        }

        //++lDoodleIndex %= 6;
    }

    // We stop the pipeline - letting the object lapse out of 
    // scope would have had the destructor do the same, but we do it anyway.
    cout << "Stop pipeline" << endl;
    lPipeline.Stop();

    // Now close the stream. Also optional but nice to have.
    cout << "Closing stream" << endl;
    lStreamGEV.Close();

    // Unregistered pipeline event sink. Optional but nice to have.
    lPipeline.UnregisterEventSink( &lPipelineEventSink );

    return true;
}


//
// Main function
//

int main(int argc, char * argv[])
{

	if (argc < 3) {
		cout << "Usage: " << argv[0] << "<device ip> <local ip> <local port>" << endl;
		return 1;
	}
	
	const std::string device_ip (argv[1]);
	const std::string local_ip (argv[2]);
	const std::string local_port_string(argv[3]);
	const int local_port (std::stoi(local_port_string));

    cout << "Receiver example" << endl << endl;
    StartSlave(device_ip, local_ip, local_port);

    cout << endl;

    return 0;
}

