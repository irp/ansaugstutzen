#ifndef MY_TIFF_IO_H
#define  MY_TIFF_IO_H

#include <vector>
#include <cstdint>
#include <iostream>
#include <tiffio.h>
#include <memory>

template <class T>
struct TIFFImage {
  int width;
  int height;

  std::vector<T> data;

  TIFFImage(std::vector<T>&& data, int width, int height)
    : width (width), height (height), data (data) {};

  // move constructor
  TIFFImage(TIFFImage&& ) = default;
  // move assignment operator
  TIFFImage& operator=(TIFFImage &&) = default;
};

void WriteToTiff(std::vector<float> const& datavec, int width, int height, std::string const& imgname) {
  const int samplesperpixel = 1;
  const int bitspersample = 32;

  const float *data = datavec.data();

  TIFF * tif = TIFFOpen(imgname.c_str(),"w");
  if (tif) {
    TIFFSetField(tif, TIFFTAG_IMAGEWIDTH, width);
    TIFFSetField(tif, TIFFTAG_IMAGELENGTH, height);
    TIFFSetField(tif, TIFFTAG_SAMPLESPERPIXEL, samplesperpixel);
    TIFFSetField(tif, TIFFTAG_BITSPERSAMPLE, bitspersample);
    TIFFSetField(tif, TIFFTAG_ORIENTATION, ORIENTATION_BOTLEFT); 
    TIFFSetField(tif, TIFFTAG_PLANARCONFIG, PLANARCONFIG_CONTIG);
    TIFFSetField(tif, TIFFTAG_PHOTOMETRIC, PHOTOMETRIC_MINISBLACK);
    TIFFSetField(tif, TIFFTAG_SAMPLEFORMAT, SAMPLEFORMAT_IEEEFP);
    TIFFSetField(tif, TIFFTAG_ROWSPERSTRIP, TIFFDefaultStripSize(tif, width*samplesperpixel));

    for(int irow=0; irow<height; irow++){
      TIFFWriteScanline(tif, const_cast<float*>(&(data[irow * width])),irow,0);
    }

    TIFFClose(tif);
  } else
    std::cout << "could not open file " << imgname << " for writing " << std::endl;

};

std::unique_ptr<TIFFImage<float>> ReadFromTiff(std::string const& imgname){

  TIFF * tif = TIFFOpen(imgname.c_str(),"r");
  if (!tif){
    std::cout << "could not open file " << imgname << " for reading " << std::endl;
    return nullptr;
  }
 
  uint16_t bps = 32;
  uint16_t sampleperpixel=1;
  uint32_t ncol;
  uint32_t nrow;

  TIFFGetField(tif, TIFFTAG_IMAGEWIDTH, &ncol);
  TIFFGetField(tif, TIFFTAG_IMAGELENGTH, &nrow);
  TIFFSetField(tif, TIFFTAG_SAMPLESPERPIXEL, sampleperpixel);
  TIFFSetField(tif, TIFFTAG_BITSPERSAMPLE, bps);

  float *imgData= reinterpret_cast<float*>(_TIFFmalloc(ncol*nrow * sizeof(float)));
  for(uint32_t irow=0; irow<nrow; irow++){
    TIFFReadScanline(tif, &imgData[irow*ncol], irow);
  }
  TIFFClose(tif);
  std::vector<float> data (imgData, imgData+ncol*nrow);
  free(imgData);

  return std::make_unique<TIFFImage<float>> (std::move(data), ncol, nrow);
};
#endif

