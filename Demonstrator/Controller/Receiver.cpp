//
// To receive images using PvPipeline
//

#include <PvStream.h>
#include <PvPipeline.h>
#include <PvBuffer.h>
#include <PvStreamGEV.h>

#include <iostream>
#include <mutex>
#include "ImageContainer.h"

#include "Receiver.h"


const int buffer_count = 16;
const int timeout = 100;
const int payload_size = 20000000;

using namespace std;

//
// PvPipeline event handler class. Used to trap buffer too small events
// and let the pipeline know we want all the buffers to be resized immediately.
//

class PipelineEventSink : public PvPipelineEventSink
{
protected:

    void OnBufferTooSmall( PvPipeline *aPipeline, bool *aReallocAll, bool *aResetStats )
    {
        *aReallocAll = true;
        *aResetStats = true;

        cout << "Buffers reallocated by the pipeline" << endl;
    }
};



void Receiver::stop() {
	run = false;
};

void Receiver::start()
{
    // Create the PvStreamGEV object.
    PvStreamGEV lStreamGEV;

    // Create the PvPipeline object.
    PvPipeline lPipeline( &lStreamGEV );
    

    // Create a PvPipeline event sink (used to trap buffer too small events).
    PipelineEventSink lPipelineEventSink;
    lPipeline.RegisterEventSink( &lPipelineEventSink );

    // Open stream
    cout << "Opening stream from " << device_ip << endl;
    
    if ( !lStreamGEV.Open( device_ip.c_str(), local_port ).IsOK() )
    {
        cout << "Error opening stream" << endl;
        return;
    }

    // IMPORTANT: the pipeline needs to be "armed", or started before 
    // we instruct the device to send us images.
    cout << "Starting pipeline" << endl;
    lPipeline.SetBufferSize(payload_size);
    lPipeline.SetBufferCount( buffer_count );
    lPipeline.Start();

    // Get stream parameters/stats.
    PvGenParameterArray *lStreamParams = lStreamGEV.GetParameters();
    PvGenInteger *lBlockCount = dynamic_cast<PvGenInteger *>( lStreamParams->Get( "BlockCount" ) );
    PvGenFloat *lFrameRate = dynamic_cast<PvGenFloat *>( lStreamParams->Get( "AcquisitionRate" ) );
    PvGenFloat *lBandwidth = dynamic_cast<PvGenFloat *>( lStreamParams->Get( "Bandwidth" ) );
    PvGenBoolean *lRequestMissingPackets = dynamic_cast<PvGenBoolean *>( lStreamParams->Get( "RequestMissingPackets" ) );

    // Disabling request missing packets.
    lRequestMissingPackets->SetValue( false );

    int64_t lBlockCountVal = 0;
    double lFrameRateVal = 0.0;
    double lBandwidthVal = 0.0;

    // Acquire images until the user instructs us to stop.
    cout << endl;
    while ( run )
    {
		// handle commands here
		
        // Retrieve next buffer     
        PvBuffer *lBuffer = nullptr;
        PvResult  lOperationResult;
        PvResult lResult = lPipeline.RetrieveNextBuffer( &lBuffer, timeout, &lOperationResult );
        
        if ( lResult.IsOK() )
        {
            if (lOperationResult.IsOK())
            {
                //
                // We now have a valid buffer. This is where you would typically process the buffer.
                // -----------------------------------------------------------------------------------------
                // ...

                lBlockCount->GetValue( lBlockCountVal );
                lFrameRate->GetValue( lFrameRateVal );
                lBandwidth->GetValue( lBandwidthVal );
            
                // If the buffer contains an image, display width and height.
                uint32_t lWidth = 0, lHeight = 0;
                if ( lBuffer->GetPayloadType() == PvPayloadTypeImage )
                {
                    // Get image specific buffer interface.
                    PvImage *lImage = lBuffer->GetImage();
                    
                    ImageContainerU16 image;
                    image.set_image(*lImage);
                    std::string filename = "image " + std::to_string(lBuffer->GetBlockID()) + ".tiff";
                    image.write_to_tiff(filename);

                    // Read width, height
                    lWidth = lImage->GetWidth();
                    lHeight = lImage->GetHeight();
                }
            
                //cout << fixed << setprecision( 1 );
                //cout << lDoodle[ lDoodleIndex ];
                cout << " BlockID: " << uppercase << hex << lBuffer->GetBlockID() << " W: " << dec << lWidth << " H: " 
                     << lHeight << " " << lFrameRateVal << " FPS " << ( lBandwidthVal / 1000000.0 ) << " Mb/s" << endl;
                     
                // NOTIFY controller
                notifier.notify(id);
                     
            }

            // We have an image - do some processing (...) and VERY IMPORTANT,
            // release the buffer back to the pipeline.
            lPipeline.ReleaseBuffer( lBuffer );
        }
        else
        {
            // Timeout
            //cout << " Timeout" << endl;
        }
    }

    // We stop the pipeline - letting the object lapse out of 
    // scope would have had the destructor do the same, but we do it anyway.
    cout << "Stop pipeline" << endl;
    lPipeline.Stop();

    // Now close the stream. Also optional but nice to have.
    cout << "Closing stream" << endl;
    lStreamGEV.Close();

    // Unregistered pipeline event sink. Optional but nice to have.
    lPipeline.UnregisterEventSink( &lPipelineEventSink );

    return;
}

