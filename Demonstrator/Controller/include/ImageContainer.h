#include <algorithm>

#include <eigen3/Eigen/Dense>
#include <PvBuffer.h>
#include <iostream>

#include <tiffio.h>
//#include <memory>
// TODO: fur andere pixelgroessen verallgemeinern

using ImageDataU16 = Eigen::Array<uint16_t, Eigen::Dynamic, Eigen::Dynamic>;

struct ImageContainerU16 {
	ImageDataU16 image;

	void set_image(const PvImage & pvimage) {
		const size_t width = pvimage.GetWidth();
		const size_t height = pvimage.GetHeight();
		const uint8_t *ptr = pvimage.GetDataPointer();

		const size_t size = width * height * sizeof(uint16_t);

		image.resize(width, height);
		std::copy(ptr, ptr + size, reinterpret_cast<uint8_t *>(image.data()));
	};
	
	void write_to_tiff(const std::string & imgname) {
  const int samplesperpixel = 1;
  const int bitspersample = 16;
  const size_t width = image.cols();
  const size_t height = image.rows();

  const uint16_t *data = image.data();

  TIFF * tif = TIFFOpen(imgname.c_str(),"w");
  if (tif) {
    TIFFSetField(tif, TIFFTAG_IMAGEWIDTH, width);
    TIFFSetField(tif, TIFFTAG_IMAGELENGTH, height);
    TIFFSetField(tif, TIFFTAG_SAMPLESPERPIXEL, samplesperpixel);
    TIFFSetField(tif, TIFFTAG_BITSPERSAMPLE, bitspersample);
    TIFFSetField(tif, TIFFTAG_ORIENTATION, ORIENTATION_BOTLEFT); 
    TIFFSetField(tif, TIFFTAG_PLANARCONFIG, PLANARCONFIG_CONTIG);
    TIFFSetField(tif, TIFFTAG_PHOTOMETRIC, PHOTOMETRIC_MINISBLACK);
    TIFFSetField(tif, TIFFTAG_SAMPLEFORMAT, SAMPLEFORMAT_UINT);
    TIFFSetField(tif, TIFFTAG_ROWSPERSTRIP, TIFFDefaultStripSize(tif, width*samplesperpixel));

    for(int irow=0; irow<height; irow++){
      TIFFWriteScanline(tif, const_cast<uint16_t*>(&(data[irow * width])),irow,0);
    }

    TIFFClose(tif);
  } else
    std::cout << "could not open file " << imgname << " for writing " << std::endl;

};
	
};
