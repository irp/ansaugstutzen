#include <atomic>
#include <condition_variable>
#include <mutex>
#include <vector>

struct Notifier {
	
	std::mutex m;
	std::condition_variable cond_var;
	std::vector<int> received_images;
	
	Notifier(size_t num_receivers) 
	 : received_images(num_receivers, 0)
	{};
	
	void reset() { 
		received_images.assign(received_images.size(), 0);
		};
	void notify(size_t receiver_id) {
		 std::unique_lock<std::mutex> lock(m);
		 received_images[receiver_id] += 1;
		 cond_var.notify_one();
		};
		
	bool all_received_image() {
		for (auto i_received : received_images) {
			if (i_received == 0)
				return false;
		}
		return true;
	}
	
};


class Receiver {
	
	public:
		Receiver(std::string const& device_id, std::string const& aIP, int aPort, Notifier & notifier, int id) 
		: device_ip(device_id), 
		local_ip(aIP), 
		local_port(aPort), 
		run (true) ,
		notifier (notifier),
		id (id)
		{};
		
		void start();
		
		void stop();
	
	private:
	    std::string device_ip;
	    std::string local_ip;
	    int local_port;
		std::atomic_bool run;
		Notifier & notifier; 
		int id;
	
};

