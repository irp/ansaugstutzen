#include <PvDeviceGEV.h>
#include <iostream>

#include <thread>
#include <chrono>

#include "Receiver.h"

//
// Shows how to control a multicast master without receiving data
//

const int packet_size = 1440;

const int deadtime = 2000;

const std::string software_trigger ("SW_Trigger");

using namespace std;

std::string get_from_device(PvGenParameterArray *aDeviceParams, std::string const& param) {
	PvGenParameter *para = aDeviceParams->Get( param.c_str() );
	PvString value;
	para->ToString(value);
	
	std::string ret (value);
	// TODO: some sort of error handling

	return ret;
}

void set_on_device(PvGenParameterArray *aDeviceParams, std::string const& param, std::string const& value) {
		PvGenParameter *para = aDeviceParams->Get( param.c_str() );
		PvResult result = para->FromString(value.c_str());
		
		// TODO: some sort of error handling
}

void set_on_device(PvGenParameterArray *aDeviceParams, std::string const& param, int value) {
	
		PvResult result = aDeviceParams->SetIntegerValue( param.c_str(), value );
		
		// TODO: some sort of error handling
}

// Commands:

// StartScan

// EndScan

// Count <ms>

// Trigger <ms>

// SetOutputDir

// GetOutputDir

// SetIndex

// GetIndex

// WaitForImage

// (SetGigE)

// (GetGigE)


Notifier notifier(1);


bool StartMaster(std::string const& device_ip, std::string const& local_ip, int local_port)
{

    // Connect to the GigE Vision device
    PvDeviceGEV lDevice;
    cout << "Connecting to " << device_ip << endl;
    if ( !lDevice.Connect( device_ip.c_str(), PvAccessControl ).IsOK() )
    {
        cout << "Error connecting device" << endl;
        return false;
    }



    // Get device parameters need to control streaming
    PvGenParameterArray *lDeviceParams = lDevice.GetParameters();
    PvGenCommand *lStart = dynamic_cast<PvGenCommand *>( lDeviceParams->Get( "AcquisitionStart" ) );
    PvGenCommand *lStop = dynamic_cast<PvGenCommand *>( lDeviceParams->Get( "AcquisitionStop" ) );
    PvGenCommand *lTrigger = dynamic_cast<PvGenCommand *> ( lDeviceParams->Get( "TriggerSoftware" ) );
    //PvGenEnum *lFault =  dynamic_cast<PvGenEnum *>(lDeviceParams->Get( "Fault" ));
    PvGenInteger *lGevSCPSPacketSize = dynamic_cast<PvGenInteger *>( lDeviceParams->Get( "GevSCPSPacketSize" ) );


	// show device info
	std::string vendor_name = get_from_device(lDeviceParams, "DeviceVendorName");
	std::string model_name = get_from_device(lDeviceParams, "DeviceModelName");
	std::string manufacturer_info = get_from_device(lDeviceParams, "DeviceManufacturerInfo");
	std::string version = get_from_device(lDeviceParams, "DeviceVersion");

	cout << "Connected to device:" << endl;
	cout << "Vendor: " << vendor_name << endl;
	cout << "Model: " << model_name << endl;
	cout << "Info: " << manufacturer_info << endl;
	cout << "Version: " << version << endl;
	cout << endl;
	
	cout << "FaultStatus: " << get_from_device(lDeviceParams, "Fault") << endl;
	cout << endl;
	
	// set basic parameters
	// set to software trigger

	std::string trigger_mode = get_from_device(lDeviceParams, "PSL_TRIGGER_MODE");
	cout << "Trigger mode: " << trigger_mode << endl;
	
	if (trigger_mode != software_trigger) {
		cout << "Setting to " << software_trigger << endl;
		set_on_device(lDeviceParams, "PSL_TRIGGER_MODE", software_trigger);
	}
	
	std::string exposure_time = get_from_device(lDeviceParams, "ExposureTimeRaw");
	cout << "Exposure time: " << exposure_time << endl;
	
    cout << "Setting stream destination to " << local_ip << " port " << local_port << endl;
    PvResult lResult = lDevice.SetStreamDestination( local_ip.c_str(), local_port );

    lGevSCPSPacketSize->SetValue( packet_size );
    

    int milliseconds;
    cout << "Insert ct in milliseconds: ";
    cin >> milliseconds;
    cout << "Parsed " << milliseconds << endl;
    
    set_on_device(lDeviceParams, "ExposureTimeRaw", milliseconds);    

    // Enables stream before sending the AcquisitionStart command.
    cout << "Enable streaming on the controller." << endl;
    lDevice.StreamEnable();

        


    // Tell the device to start sending images to the multicast group
    cout << "Sending StartAcquisition command to device" << endl;
    lStart->Execute();

    cout << endl;
    
    exposure_time = get_from_device(lDeviceParams, "ExposureTimeRaw");
	cout << "Exposure time: " << exposure_time << endl;   
	
	notifier.reset();

    cout << "Executing software trigger" << endl;
    lTrigger->Execute();
    
    {
		cout << "Waiting for image" <<  endl;
		std::unique_lock<std::mutex> lock(notifier.m);
	    bool received = notifier.cond_var.wait_for(lock, std::chrono::milliseconds(milliseconds+deadtime),
	    [&](){ return notifier.all_received_image(); }	     );
	    if(received) {
			cout << "Received image" << endl;
		}else{
			cout << "Timed out..." << endl;
		}
	}
    
   	notifier.reset();
    
    cout << "Executing software trigger" << endl;
    lTrigger->Execute();
    
    {
		cout << "Waiting for image" <<  endl;
		std::unique_lock<std::mutex> lock(notifier.m);
	    bool received = notifier.cond_var.wait_for(lock, std::chrono::milliseconds(milliseconds+deadtime),
	    [&](){ return notifier.all_received_image(); }	     );
	    if(received) {
			cout << "Received image" << endl;
		}else{
			cout << "Timed out..." << endl;
		}
	}



    // Tell the device to stop sending images
    cout << "Sending AcquisitionStop command to the device" << endl;
    lStop->Execute();

    // Disable stream after sending the AcquisitionStop command.
    cout << "Disable streaming on the controller." << endl;
    lDevice.StreamDisable();

    // Disconnect the device. Optional, still nice to have
    cout << "Disconnecting device" << endl;
    lDevice.Disconnect();

    return true;
}

//
// Main function
//

int main(int argc, char * argv[])
{

	if (argc < 3) {
		
		cout << "Usage: " << argv[0] << "<device ip> <local ip> <local port>" << endl;
		return 1;
	}
	
	const std::string device_ip (argv[1]);
	const std::string local_ip (argv[2]);
	const std::string local_port_string(argv[3]);
	const int local_port (std::stoi(local_port_string));
    cout << "Controller example" << endl;
	
	Receiver receiver (device_ip, local_ip, local_port, notifier, 0);
	
	// receiver ist ein eigener thread
	std::thread receiver_thread (&Receiver::start, &receiver);
	
    StartMaster(device_ip, local_ip, local_port);
    

	receiver.stop();
	
	receiver_thread.join();

    cout << endl;

    return 0;
}

